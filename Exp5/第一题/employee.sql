/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2018-10-15 00:27:49
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `employee`
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `Emp_id` int(5) NOT NULL AUTO_INCREMENT,
  `EMP_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `JOB` varchar(10) NOT NULL,
  `SALARY` double(20,2) NOT NULL,
  `DEPT` int(5) NOT NULL,
  PRIMARY KEY (`Emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO employee VALUES ('1', '王楠', 'clerk', '4500.00', '10');
INSERT INTO employee VALUES ('2', '张敬', 'clerk', '4300.00', '10');
INSERT INTO employee VALUES ('3', '李刚', 'clerk', '5000.00', '20');
INSERT INTO employee VALUES ('14', '玛丽', 'clerk', '5500.00', '10');
