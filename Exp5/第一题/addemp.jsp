<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>添加员工</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
<style type="text/css">
	h1{
		color:blue;
		text-align: center;
		letter-spacing: 2px;
	}
	.td1{
		height:40px;
		color: blue;
		font-size:20px;
	}
	div{
		width:400px;
		margin:0 auto;
		color: blue;
		font-size:20px;
	}
</style> 
  <body>
  ${AddEmp}
  	<h1>添加员工</h1>
  	<form action="addEmpAction" method="post"> 
  		<table style="width:300px; margin:0 auto;">
			<tr>
				<td class="td1">员工姓名：</td>
				<td><input type="text" name="EMP_NAME"></td>
			</tr>
			<tr>
				<td class="td1">职&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;称：</td>
				<td><input type="text" name="JOB"></td>
			</tr>
			<tr>
				<td class="td1">工&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;资：</td>
				<td><input type="text" name="SALARY"></td>
			</tr>
			<tr>
				<td class="td1">部门编号：</td>
				<td><input type="text" name="DEPT"></td>
			</tr>
			<tr>
				<td align="right">
					<input type="submit" value="添加员工">
				</td>
				<td align="center">
					<input type="reset" value="重置">
				</td>
			</tr>
		</table>
		<br>
		<div>
			查询请选择员工工作种类：
			<select name="job">
				<option value="clerk">clerk</option>
				<option value="manager">manager</option>
			</select>
		</div>
  	</form>
  </body>
</html>
