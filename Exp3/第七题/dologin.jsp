<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:useBean id="loginUser" class="Dao.Users" scope="page"></jsp:useBean>
<jsp:useBean id="userDao" class="Dao.UserDao"></jsp:useBean>
<jsp:setProperty property="*" name="loginUser"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'dologin.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
   <%
    if(userDao.userLogin(loginUser)){
     session.setAttribute("username",loginUser.getUsername());
     request.getRequestDispatcher("login_success.jsp").forward(request,response);
    }
    else{
    response.sendRedirect("login_failure.jsp");
    }
    %>
  </body>
</html>
