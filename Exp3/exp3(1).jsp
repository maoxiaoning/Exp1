<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'index.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script>
    function getresult()
    {
        var num1=document.getElementById("num1").value;
        var sig=document.getElementById("sig").value;
        var num2=document.getElementById("num2").value;
        n1 = parseFloat(num1);
        n2 = parseFloat(num2);
         if(sig=="/"&& num2=="0")
        {
                document.write("除数不能为零");
            
        }
        else
        {
            
     switch (sig) {
            case "+":
                document.getElementById("result").value = n1+n2;
                break;
            case "-":
                document.getElementById("result").value =n1-n2;
                break;
            case "*":
                document.getElementById("result").value =n1*n2;
                break;
            case "/":
                document.getElementById("result").value =n1/n2;
                break;

            } 
        }

    }
</script>
	<style type="text/css">
	h1{
	 margin:25px 10px;	 
	}
	.line{
	margin:15px 10px;
	}
	.main{
	margin-top:30px;
	}
	</style>
  </head>
  
  <body>
    <h1>我的计算器</h1>
    <hr>
    <div class="main">
      <div class="line">
        请输入第一个数：<input type="text" name="num1" id="num1">
      </div>
      <div class="line">
      请选择运算方式：<select name="sig" id="sig">
       <option  value="+">+</option>
       <option  value="-">-</option>
       <option  value="*">*</option>
       <option  value="/">/</option>
      </select>
      </div>
      <div class="line">
      请输入第二个数：<input type="text" name="num2" id="num2">
      </div>
      <div class="line">
      <input type="submit" value="计算" onclick="getresult()">
      <input type="reset" value="重置">
      </div>
      <div class="line">
      结果：<input type="text" name="result" id="result">
      </div>
    </div>
  </body>
</html>
