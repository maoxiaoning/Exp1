<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'index.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body>
<%
 if(pageContext.getAttribute("pageCount")==null){
    pageContext.setAttribute("pageCount",new Integer(0));
 }
 if(session.getAttribute("sessionCount")==null){
   session.setAttribute("sessionCount",new Integer(0));
 }
 Integer count=(Integer)pageContext.getAttribute("pageCount");
 pageContext.setAttribute("pageCount",new Integer(
 count.intValue()+1));
 Integer count2=(Integer)session.getAttribute("sessionCount");
 session.setAttribute("sessionCount",new Integer(
  count2.intValue()+1));
 %>
 页面计数：<%=pageContext.getAttribute("pageCount") %>
浏览器计数<%=session.getAttribute("sessionCount") %>
  </body>
</html>
