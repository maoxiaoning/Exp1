package com.web;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.UserDao;
import com.model.User;
import com.util.Dbutil;

public class LoginServlet extends HttpServlet {
	Dbutil db=new Dbutil();
	UserDao userDao=new UserDao();
   
	private static final long serialVersionUID =1L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       this.doPost(request, response);
   }

     @Override
     protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         String username=request.getParameter("username");
         String password=request.getParameter("password");
         Connection con=null;
         try {
             User user=new User(username,password);
             con=db.getCon();
             User currentUser=userDao.login(con, user);
             if(currentUser==null){
                 //System.out.println("no");
                 request.getRequestDispatcher("/error.jsp").forward(request, response);
             }else{
                 request.getRequestDispatcher("/success.jsp").forward(request,response);
             }
        } catch (Exception e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         }
         
     }
     
}
