<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'login.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <jsp:useBean id="usersLogin" class="user.User"></jsp:useBean>
  <jsp:setProperty property="*" name="usersLogin"/>
<% 
session.setAttribute("User", usersLogin);
String []hobbies=request.getParameterValues("hobbies");
String str="";
	for(int i=0;i<hobbies.length;i++){
		str+=hobbies[i]+' ';
	}
pageContext.setAttribute("hobbies",str);
%>
<div>
  <form action="index.jsp" method="post">
    <table>
     <tr>
      <td>
         用户名称：${User.username}
      </td>
     </tr>
     <tr>
      <td>
  密码：${User.password}
      </td>
     </tr>
     <tr>
      <td>
 性别：${User.sex}
      </td>
     </tr>
     <tr>
      <td>
       爱好：${hobbies}
      </td>
     </tr>
     <tr>
      <td>
  <input type="submit" value="返回">
      </td>
     </tr>
    </table>
  </form>
</div>
  </body>
</html>
