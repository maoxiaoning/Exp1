<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'index.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
	 .td{
	  text-align:right;
	 }
	 table{
	 width:500px;
	 margin:30px auto;
	 }
	 tr{
	 margin-top:10px;
	 }
	</style>
  </head> 
  <body>
  <form action="login.jsp">
   <table>
     <tr>
       <td class="td">用户昵称：</td>
       <td><input type="text" name="username"></td>
     </tr>
      <tr>
       <td  class="td">密码：</td>
       <td><input type="password" name="password"></td>
     </tr>
      <tr>
       <td  class="td">确认密码：</td>
       <td><input type="password" name="repassword"></td>
     </tr>
     <tr>
       <td  class="td">性别：</td>
       <td><input type="radio" value="man" name="sex" checked>男<input type="radio" value="female" name="sex">女</td>
     </tr>
     <tr>
       <td  class="td">爱好：</td>
       <td><input type="checkbox" value="sport" name="hobbies">体育<input type="checkbox" value="art" name="hobbies">美术<input type="checkbox" value="music" name="hobbies">音乐<input type="checkbox" value="travelling" name="hobbies">旅游</td>
     </tr>
     <tr>
       <td  class="td"><input type="submit" value="提交"></td>
       <td><input type="reset"></td>
     </tr>
   </table>
  </form>
  </body>
</html>
